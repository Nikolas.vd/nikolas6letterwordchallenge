﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _6LetterWordChallenge
{
    internal class WordCombinator
    {
        public List<string> FindWordCombinations(Dictionary<int, HashSet<string>> wordDictionary, int targetLength)
        {
            List<string> combinations = new List<string>();

            // Iterate through each list of words in the dictionary up to half of the target length
            for (int i = 1; i <= targetLength / 2; i++)
            {
                // Check if there's a list of words with length i and complement length
                int complementLength = targetLength - i;
                if (wordDictionary.ContainsKey(i) && wordDictionary.ContainsKey(complementLength))
                {
                    // Combine words from the current list and the complement list
                    var currentWords = wordDictionary[i];
                    var complementWords = wordDictionary[complementLength];

                    foreach (var word1 in currentWords)
                    {
                        foreach (var word2 in complementWords)
                        {
                            string combinedWord = word1 + word2;
                            if (wordDictionary.ContainsKey(targetLength) && wordDictionary[targetLength].Contains(combinedWord))
                            {
                                combinations.Add($"{word1}+{word2}={combinedWord}");
                            }
                        }
                    }
                }
            }

            // If the target length is odd, also check combinations from the middle list
            if (targetLength % 2 != 0 && wordDictionary.ContainsKey(targetLength / 2))
            {
                var middleWords = wordDictionary[targetLength / 2];

                foreach (var word1 in middleWords)
                {
                    foreach (var word2 in middleWords)
                    {
                        string combinedWord = word1 + word2;
                        if (wordDictionary.ContainsKey(targetLength) && wordDictionary[targetLength].Contains(combinedWord))
                        {
                            combinations.Add($"{word1}+{word2}={combinedWord}");
                        }
                    }
                }
            }

            return combinations;
        }

    }
}
