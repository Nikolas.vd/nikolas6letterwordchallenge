﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _6LetterWordChallenge
{
    public class InputFileReader
    {
        public Dictionary<int, HashSet<string>> ReadWordsFromFile(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                {
                    Console.WriteLine("Error: Input file does not exist.");
                    return null;
                }

                // Read all lines from the input file
                string[] lines = File.ReadAllLines(filePath);

                // Check if the file is empty
                if (lines.Length == 0)
                {
                    Console.WriteLine("Error: Input file is empty.");
                    return null;
                }

                // Create a dictionary to store words organized by length
                Dictionary<int, HashSet<string>> wordDictionary = new Dictionary<int, HashSet<string>>();

                // Iterate through each line and add words to the dictionary
                foreach (string line in lines)
                {
                    // Split the line into words
                    string[] lineWords = line.Split(' ');

                    // Add each word to the dictionary based on its length
                    foreach (string word in lineWords)
                    {
                        int length = word.Length;

                        // Check if the dictionary already contains a hash set for the current length
                        if (!wordDictionary.ContainsKey(length))
                        {
                            // If not, create a new hash set for words of this length
                            wordDictionary[length] = new HashSet<string>();
                        }

                        // Add the word to the hash set for its length
                        wordDictionary[length].Add(word);
                    }
                }

                return wordDictionary;
            }
            catch (IOException ex)
            {
                // Handle file IO exceptions
                Console.WriteLine($"Error reading input file: {ex.Message}");
                return null;
            }
            catch (Exception ex)
            {
                // Handle any other exceptions
                Console.WriteLine($"An error occurred: {ex.Message}");
                return null;
            }
        }
    }
}
