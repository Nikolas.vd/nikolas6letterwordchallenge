﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _6LetterWordChallenge
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            bool restart = true;
            while (restart)
            {
                // Check if input file path is provided
                if (args.Length < 1)
                {
                    Console.WriteLine("Usage: YourApplication.exe <inputFilePath>");
                    return;
                }

                string inputFilePath = args[0];

                if (!System.IO.File.Exists(inputFilePath))
                {
                    Console.WriteLine("Input file does not exist.");
                    return;
                }

                // Instantiate InputFileReader
                var inputFileReader = new InputFileReader();

                // Read the contents of the input file
                var wordDictionary = inputFileReader.ReadWordsFromFile(inputFilePath);

                // Check if words were successfully read
                if (wordDictionary != null)
                {
                    // question see the values of the input file
                    string showValuesResponse;
                    while (true)
                    {
                        Console.Write("Do you want to see the values of the input file? (yes/no): ");
                        showValuesResponse = Console.ReadLine()?.ToLower();
                        if (showValuesResponse == "no" || showValuesResponse == "n")
                        {
                            break;
                        }
                        else if (showValuesResponse == "yes" || showValuesResponse == "y")
                        {
                            Console.WriteLine("Word Dictionary:");
                            foreach (var entry in wordDictionary.OrderBy(kv => kv.Key))
                            {
                                Console.WriteLine($"{entry.Key} Letter Words:");
                                foreach (var word in entry.Value)
                                {
                                    Console.WriteLine(word);
                                }
                            }
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter 'yes' or 'no'.");
                        }
                    }

                    // question for the target length
                    int targetLength;
                    while (true)
                    {
                        int longestWordLength = wordDictionary.Keys.Max();
                        Console.Write($"Enter the target length for word combinations (must be a number between 2 and {longestWordLength}): ");
                        string targetLengthInput = Console.ReadLine();
                        if (int.TryParse(targetLengthInput, out targetLength))
                        {
                            if (targetLength >= 2 && targetLength <= longestWordLength)
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine($"Invalid input! Target length must be between 2 and {longestWordLength}.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter a valid number.");
                        }
                    }

                    // Find and print word combinations
                    var wordCombinator = new WordCombinator();
                    var combinations = wordCombinator.FindWordCombinations(wordDictionary, targetLength);

                    Console.WriteLine($"Word combinations of length {targetLength}:");
                    foreach (var combination in combinations)
                    {
                        Console.WriteLine(combination);
                    }
                    Console.WriteLine($"Number of combinations: {combinations.Count}");

                    // question restart the program or exit
                    string restartResponse;
                    while (true)
                    {
                        Console.Write("Do you want to restart the program or exit? (restart/exit): ");
                        restartResponse = Console.ReadLine()?.ToLower();
                        if (restartResponse == "restart" || restartResponse == "r")
                        {
                            Console.WriteLine();
                            break;
                        }
                        else if (restartResponse == "exit" || restartResponse == "e")
                        {
                            restart = false;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter 'restart' or 'exit'.");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Failed to read words from input file.");
                    restart = false; // Exit the program if reading the input file fails
                }
            }
        }
    }
}
