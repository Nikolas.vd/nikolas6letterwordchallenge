﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _6LetterWordChallenge.Tests
{
    [TestClass]
    public class ProgramTests
    {
        private string _testFilePath;

        [TestInitialize]
        public void Setup()
        {
            // Create a temporary test file
            _testFilePath = Path.GetTempFileName();
            File.WriteAllText(_testFilePath, "test hello world programming\nexample testing unit");
        }

        [TestCleanup]
        public void TearDown()
        {
            // Delete the temporary test file
            File.Delete(_testFilePath);
        }

        [TestMethod]
        public void ReadWordsFromFile_WhenValidFile_ReturnsWordDictionary()
        {
            // Arrange
            var inputFileReader = new InputFileReader();

            // Act
            var wordDictionary = inputFileReader.ReadWordsFromFile(_testFilePath);

            // Assert
            Assert.IsNotNull(wordDictionary);
            Assert.AreEqual(6, wordDictionary.Count);
            Assert.IsTrue(wordDictionary.ContainsKey(4));
            Assert.IsTrue(wordDictionary.ContainsKey(7));
        }

        [TestMethod]
        public void FindWordCombinations_WhenValidDictionaryAndTargetLength_ReturnsCombinations()
        {
            // Arrange
            var wordDictionary = new Dictionary<int, HashSet<string>>
            {
                { 3, new HashSet<string> { "cat", "dog", "bat" } },
                { 4, new HashSet<string> { "bird", "frog" } },
                { 6, new HashSet<string> { "monkey", "donkey" } }
            };
            var wordCombinator = new WordCombinator();

            // Act
            var combinations = wordCombinator.FindWordCombinations(wordDictionary, 7);

            // Assert
            Assert.IsNotNull(combinations);
            Assert.AreEqual(1, combinations.Count);
            Assert.AreEqual("dog+bird=dogbird", combinations.First());
        }
    }
}
